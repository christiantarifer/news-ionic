import { Injectable } from '@angular/core';

import { Storage } from '@ionic/storage-angular';
import { ToastController } from '@ionic/angular';

// ***************************** INTERFACE ************************************ //

import { Article } from '../interfaces/interfaces';

@Injectable({
  providedIn: 'root'
})
export class DataLocalService {

  noticias: Article[] = [];

  constructor( private storage: Storage,
               public toastController: ToastController
   ) {

    this.storage.create();
    this.cargarFavoritos();

   }

  guardarNoticia( noticia: Article ) {

    const existe = this.noticias.find( noti => noti.title === noticia.title );

    if ( !existe ) {

      this.noticias.unshift( noticia );
      this.storage.set( 'favoritos', this.noticias );
      this.presentToast( 'Noticia guarda en favoritos' );

    }


  }

  async cargarFavoritos() {

    const favoritos = await this.storage.get( 'favoritos' );

    if ( favoritos ) {

      this.noticias = favoritos;


    }

  }

  borrarNoticia( noticia: Article ) {

    this.noticias = this.noticias.filter( noti => noti.title !== noti.title );
    this.storage.set( 'favoritos', this.noticias );
    this.presentToast( 'Noticia eliminada en favoritos' );

  }

  async presentToast( message: string) {
    const toast = await this.toastController.create({
      message,
      duration: 2000
    });
    toast.present();
  }


}
