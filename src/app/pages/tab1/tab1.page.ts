import { Component, OnInit } from '@angular/core';
import {pluck } from 'rxjs/operators';

// ******************************** SERVICES ********************************** //

import { NoticiasService } from '../../services/noticias.service';

// ******************************** INTERFACES ******************************* //

import { Article } from '../../interfaces/interfaces';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page implements OnInit {

  noticias: Article[] = [];

  constructor(private noticiasService: NoticiasService ) {

  }

  ngOnInit() {

    this.cargarNoticias();

  }

  loadData( event ){


    // console.log(event);

    this.cargarNoticias( event );

  }

  cargarNoticias( event? ) {

    this.noticiasService.getTopHeadlines()
      .pipe( pluck('articles') )
      .subscribe(

        resp => {

          // console.log('noticias', resp);

          if ( resp.length === 0 ){

            event.target.disabled = true;
            event.target.complete();
            return;

          }

          this.noticias.push( ...resp );

          if ( event ) {

            event.target.complete();

          }

        }

      );

  }


}
