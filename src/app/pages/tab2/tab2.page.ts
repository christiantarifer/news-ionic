import { Component, ViewChild, OnInit } from '@angular/core';
import { IonSegment } from '@ionic/angular';

import { pluck } from 'rxjs/operators';


// ******************************* SERVICE *************************** //

import { NoticiasService } from '../../services/noticias.service';

// ******************************* INTERFACES ****************************** //

import { Article } from '../../interfaces/interfaces';

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page implements OnInit{

  @ViewChild( IonSegment, {static: true} ) segment: IonSegment;

  categorias = [ 'business', 'entertainment', 'general', 'health', 'sicence', 'sports', 'technology' ];
  noticias: Article[] = [];

  constructor(private noticiasService: NoticiasService) {}

  ngOnInit(): void {

    this.segment.value = this.categorias[0];

    this.cargarNoticias( this.segment.value );

  }

  cambioCategoria( event ){

    // console.group('Cambio de categoria');
    // console.log( event.detail.value );
    // console.groupEnd();

    this.noticias = [];

    this.cargarNoticias( event.detail.value );

  }

  cargarNoticias( categoria: string, event? ) {

    this.noticiasService.getTopHeadlinesCategoria( categoria )
      .pipe( pluck('articles'))
      .subscribe( resp => {

        // console.log(resp);
        this.noticias.push( ...resp );

        if ( event ) {

          event.target.complete();

        }

      });

  }

  loadData( event) {

    this.cargarNoticias( this.segment.value, event );

  }

}
