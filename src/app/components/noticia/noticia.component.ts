import { Component, Input, OnInit } from '@angular/core';
import { ActionSheetController } from '@ionic/angular';

// ********************************* NATIVE PLUGINS ******************************** //

import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { SocialSharing } from '@ionic-native/social-sharing/ngx';

// ********************************** INTERFACE *********************************** //

import { Article } from '../../interfaces/interfaces';

// ********************************** SERVICE ************************************ //

import { DataLocalService } from '../../services/data-local.service';


// ******************************************************************************* //
@Component({
  selector: 'app-noticia',
  templateUrl: './noticia.component.html',
  styleUrls: ['./noticia.component.scss'],
})
export class NoticiaComponent implements OnInit {

  @Input() noticia: Article;
  @Input() i: number;
  @Input() enFavoritos;

  constructor( private iab: InAppBrowser,
               private actionSheetCtrl: ActionSheetController,
               private socialSharing: SocialSharing,
               private dataLocalService: DataLocalService
             ) { }

  ngOnInit() {

    console.log('Favoritos', this.enFavoritos);

  }

  abrirNoticia() {

    const browser = this.iab.create( this.noticia.url, '_system' );

  }

  async lanzarMenu() {

    let guardarBorrarBtn;

    if ( this.enFavoritos ) {

      // DELETE FROM FAVOURITE
      guardarBorrarBtn = {
        text: 'Borrar Favorito',
        icon: 'trash-outline',
        handler: () => {
          console.log('Borrar Favorito');
          this.dataLocalService.borrarNoticia( this.noticia );
        }
      };


    } else {

      guardarBorrarBtn = {
        text: 'Favorito',
        icon: 'star',
        handler: () => {
          console.log('Favorito');
          this.dataLocalService.guardarNoticia( this.noticia );
        }
      };

    }

    const actionSheet = await this.actionSheetCtrl.create({

      cssClass: 'my-custom-class',
      buttons: [
      {
        text: 'Compartir',
        icon: 'share',
        handler: () => {
          console.log('Share clicked');
          this.socialSharing.share(
            this.noticia.title,
            this.noticia.source.name,
            '',
            this.noticia.url
          );
        }
      },
      guardarBorrarBtn,
      {
        text: 'Cancel',
        icon: 'close',
        role: 'cancel',
        handler: () => {
          console.log('Cancel clicked');
        }
      }]

    });

    await actionSheet.present();

  }

}
